---
layout: default
title: Internationalization
permalink: /dev/locales
parent: Hacking
nav_order: 7
---

## Internationalization 

We're self-hosting an instance of [weblate](https://weblate.gancio.org) you can use to help us with translations.
Currently supported languages:  



<a href="http://weblate.gancio.org/engage/gancio/?utm_source=widget">
<img src="http://weblate.gancio.org/widgets/gancio/-/multi-auto.svg" alt="Stato traduzione" />
</a>